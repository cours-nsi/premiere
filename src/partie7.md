<h1 style="text-align:center;"> Bibliographie et sitographie </h1>

<br>
<div style="text-align: center;"> 
<img src = "biblio.png" alt='image'>
</div>
<br><hr>

<h2>Bibliographie</h2>
<ul>
    <li>Algorithmique :
        <ul>
            <li><a href='https://www.eyrolles.com/Informatique/Livre/introduction-a-la-science-informatique-9782866311889/'>G. Dowek, <span style='font-style:italic;'>Introduction à la science informatique</span>, RPA, Scérén, CNDP-CRDP (2011)</a></li>
            <li><a href='https://www.dunod.com/sciences-techniques/algorithmique-cours-avec-957-exercices-et-158-problemes'>T. H. Cormen, C. Leiserson, R. Rivest, C. Stein, <span style='font-style:italic;'>Algorithmique</span>, 3ème édition, Sciences Sup, Dunod (2010)</a></li>
        </ul>
    </li>
    <li>Architecture des ordinateurs :
        <ul>
            <li><a href='https://www.eyrolles.com/Informatique/Livre/introduction-a-la-science-informatique-9782866311889/'>G. Dowek, <span style='font-style:italic;'>Introduction à la science informatique</span>, RPA, Scérén, CNDP-CRDP (2011)</a></li>
            <li><a href='https://www.eyrolles.com/Informatique/Livre/informatique-et-sciences-du-numerique-specialite-isn-en-terminale-s-avec-des-ex-9782212135435/'>G. Dowek et al., <span style='font-style:italic;'>Informatique et sciences du numériques, Spécialité ISN en terminale S</span>, Eyrolles (2012)</a></li>
            <li><a href='https://www.editions-hermann.fr/livre/from-transistor-to-computer-claude-timsit'>C. Timsit et S. Zertal, <span style='font-style:italic;'>From transistor to computer: an introduction to the world of computer architecture</span>, Hermann (2013)</a></li>
        </ul>
    </li>
    <li>Programmation Python :
        <ul>
            <li><a href='https://www.dunod.com/sciences-techniques/programmation-en-python-pour-mathematiques-0'>A. Casamayou-Boucau, P. Chauvin et G. Connan, <span style='font-style:italic;'>Programmation en Python pour les mathématiques</span>, 2ème édition, Dunod (2016)</a></li>
            <li><a href='https://www.eyrolles.com/Informatique/Livre/informatique-et-sciences-du-numerique-specialite-isn-en-terminale-s-avec-des-ex-9782212135435/'>G. Dowek et al., <span style='font-style:italic;'>Informatique et sciences du numériques, Spécialité ISN en terminale S</span>, Eyrolles (2012)</a></li>
            <li><a href='http://inforef.be/swi/python.htm'>G. Swinnen, <span style='font-style:italic;'>Apprendre à programmer avec Python 3</span> (en ligne)</a></li>
        </ul>
    </li>
    <li>Livres essentiels
        <ul>
            <li><a href='https://www.le-passeur-editeur.com/les-livres/essais/les-oubli%C3%A9es-du-num%C3%A9rique/'>Les oubliées du numérique, Editeur Le Passeur (2019)</a> Un livre essentiel pour comprendre pourquoi le numérique est massivement dominé par les hommes et quelles sont les solutions à mettre en place pour l’inclusion des femmes dans ce secteur, un enjeu crucial aujourd’hui.</li> 
        </ul>
        <ul>
            <li><a href='https://www.ins2i.cnrs.fr/fr/les-decodeuses-du-numerique'>Les décodeuses du numérique</a> Au travers de 12 portraits de chercheuses, enseignantes-chercheuses et ingénieures dans les sciences du numérique, croquées par le crayon de Léa Castor, l'Institut des sciences de l'information et de leurs interactions (INS2I) du CNRS a souhaité mettre en avant la diversité des recherches en sciences du numérique et contribuer à briser les stéréotypes qui dissuadent les femmes de s’engager dans cette voie.</li> 
        </ul>
    </li>
</ul>

<br><hr>

<h2>Sitographie</h2>
<ul>
    <li>Architecture des ordinateurs :
        <ul>
            <li>Instructions en ligne de commande :
                <ul>
                    <li><a href='http://luffah.xyz/bidules/Terminus/'>Jeu Terminus</a></li>
                    <li><a href='https://bellard.org/jslinux/'>Machine virtuelle Linux</a></li>
                </ul>
            </li>
            <li>Simulation de portes logiques :
                <ul>
                    <li><a href='https://logic.modulo-info.ch/'>Logic</a></li>
                    <li><a href='http://www.cburch.com/logisim/index.html'>Logisim</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li>Développement Web :
        <ul>
            <li>Cours :
                <ul>
                    <li><a href='https://openclassrooms.com/fr/courses/1603881-creez-votre-site-web-avec-html5-et-css3'>OpenClassrooms (HTML5 et CSS3)</a></li>
                    <li><a href='https://openclassrooms.com/fr/courses/6175841-apprenez-a-programmer-avec-javascript'>OpenClassrooms (JavaScript)</a></li>
                    <li><a href='https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql'>OpenClassrooms (PHP)</a></li>
                    <li><a href='https://w3schools.com'>w3schools</a></li>
                </ul>
            </li>
            <li>Sites officiels :
                <ul>
                    <li><a href='https://developer.mozilla.org/fr/'>Développeurs de Mozilla</a></li>
                    <li><a href='https://www.javascript.com/'>JavaScript</a></li>
                    <li><a href='http://www.php.net/'>PHP</a></li>
                    <li><a href='https://www.w3.org/'>W3C</a></li>
                </ul>
            </li>
            <li>Validateurs :
                <ul>
                    <li><a href='https://jigsaw.w3.org/css-validator/#validate_by_upload'>CSS</a></li>
                    <li><a href='https://validator.w3.org/#validate_by_upload+with_options'>HTML</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li>Programmation Python :
        <ul>
            <li>Environnement de Développement Intégré (EDI)
                <ul>
                    <li><a href='https://www.anaconda.com/download'>Anaconda</a> (multiplateforme)</li>
                    <li><a href='https://console.basthon.fr/'>Basthon</a> (en ligne)</li>
                    <li><a href='https://www.edupyter.net/'>EduPyter</a> (windows)</li>
                    <li><a href='https://edupython.tuxfamily.org/'>EduPython</a> (windows)</li>
                    <li><a href='https://www.python.org/downloads/'>IDLE</a> (multiplateforme)</li>
                    <li><a href='https://www.spyder-ide.org/'>Spider</a> (multiplateforme)</li>
                </ul>
            </li>
            <li>Gestion des notebooks :
                <ul>
                    <li><a href='https://notebook.basthon.fr/'>Basthon</a></li>
                    <li><a href='https://capytale2.ac-paris.fr/web/c-auth/list'>Capytale</a></li>
                </ul>
            </li>
            <li>Sites officiels :
                <ul>
                    <li><a href='https://openclassrooms.com/courses/apprenez-a-programmer-en-python'>OpenClassrooms</a></li>
                    <li><a href='https://www.pygame.org/news'>Pygame</a></li>
                    <li><a href='https://www.python.org/doc/'>Python</a></li>
                    <li><a href='https://w3schools.com'>w3schools</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li>Réseaux :
        <ul>
            <li>Simulation de réseaux :
                <ul>
                    <li><a href='https://www.lernsoftware-filius.de/Herunterladen'>Filius</a> (site en allemand, mais possibilité de mettre le logiciel en français)</li>
                </ul>
            </li>
            </li>
        </ul>
    </li>
    <li> Sites de cours de première :
        <ul>
            <li><a href='https://www.france-ioi.org/'>France IOI : contenus gratuits pour découvrir la programmation, progresser en algorithmique et développer le goût d'apprendre et de réfléchir</a></li> 
            <li><a href='https://blog.adatechschool.fr/jeux-apprendre-a-coder/'>Jeux pour apprendre à programmer</a></li>
             <li><a href='https://py-rates.org/'>Jeu pour découvrir les bases du langage Python</a></li>
             <li><a href='https://pixees.fr/informatiquelycee/'>Site de David Roche</a></li> 
            <li><a href='https://glassus.github.io/premiere_nsi/'>Site de Gilles Lassus</a></li>                  
        </ul>
    </li>
    <li> Sites sur les sciences du numérique :
        <ul>
            <li><a href='https://interstices.info/'>Algorithmes, logiciels, modèles, données... entrez dans les interstices</a></li>
            <li><a href='https://www.inria.fr/fr'>INRIA</a></li>    
        </ul>
    </li>   
</ul>