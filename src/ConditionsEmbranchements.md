<h1 style="text-align:center;">Chapitre 4 : conditions et embranchements </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "if.jpeg" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "Fei.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Fei-Fei_Li' target="_blank">Fei-Fei Li</a>
 est informaticienne et chercheuse en intelligence artificielle. Ses activités de recherche concernent l'apprentissage artificiel, l'apprentissage profond, la vision par ordinateur et les neurosciences cognitives. Elle est la principale instigatrice de la base d'images ImageNet.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 


<br>
Dans la vie courante, on est souvent confronté à des choix et on choisit de faire telle ou telle chose suivant les circonstances que l’on
observe.
<br><br>
En algorithmique et programmation, on observe le même type de phénomène :
<p style='text-align:center;font-style:italic;'>si une condition donnée est vraie, alors je vais réaliser telles instructions ; mais, si elle est fausse, alors je poursuis ce que j’ai à faire ou je réalise d’autres instructions (qui peuvent elles-mêmes dépendre d’une nouvelle condition).</p>
Cette structure algorithmique est appelée <span style='font-style:italic;'>structure d’embranchement</span>.
<br><br>
Le but de ce chapitre est d'introduire et manipuler les notions de conditions et de tests, ainsi que les structures d'embranchement.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un point de cours pour présenter les notions de conditions et de tests ;</li>
<li>un notebook d'entraînement sur les conditions et les tests ;</li>
<li>un QCM d'auto-évaluation ;
<li>un point de cours pour présenter les structures d'embranchement ;</li>
<li>deux notebooks d'entraînement sur les structures d'embranchement ;</li>
<li>un QCM d'auto-évaluation ;</li>
<li>des exercices complémentaires facultatifs utilisant la bibliothèque <code>turtle</code>.</li>
</ol>
<br><br>

<div style="text-align: center;"> 
<img src = "cours.png">
</div>

<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','ConditionsEmbranchements.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Booleens_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;display:none;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Accès via Capytale </h3>
Dans la zone Rechercher, taper les phrases ci-dessous :

    - NSI Première Partie 1 Chapitre 4 Conditions et tests

    - NSI Première Partie 1 Chapitre 4 Embranchements simples

    - NSI Première Partie 1 Chapitre 4 Embranchements multiples

<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/82jC7NGHF957doC" target="_blank">Téléchargement des ressources</a>

