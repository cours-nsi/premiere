<h1 style="text-align:center;">Chapitre 1 : introduction à l’algorithmique</h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "algo.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "ada.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Ada_Lovelace' target="_blank">Ada Lovelace</a>
 est considérée comme la première programmeuse de l'histoire, lors de son travail sur un ancêtre de l'ordinateur : la machine analytique de Charles Babbage. Dans ses notes, on trouve en effet le premier programme publié, destiné à être exécuté par une machine, ce qui fait d'Ada Lovelace la première personne à avoir programmé au monde.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
L’<span style='font-style:italic;'>algorithmique</span> est l'étude des algorithmes. C'est l'art d'analyser des algorithmes afin d'en mesurer l'efficacité et la fiabilité.
<br><br>
Le but de ce premier chapitre est d'introduire cette branche, à cheval à la fois sur les Mathématiques et l'Informatique, et de présenter quelques questions importantes qui y sont liées.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un point de cours présentant quelques questions importantes liées à l'écriture des algorithmes : spécifications, tests, preuve et complexité ;</li>
<li>des exercices d'entraînement pour manipuler les spécifications et les tests ;</li>
<li>un notebook d'approfondissement sur les spécifications et les tests.</li>
</ol>
<br><br> 


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','IntroAlgo_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','IntroAlgo_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>


<h3>Accès via Capytale</h3>

Dans la zone Rechercher, taper la phrase ci-dessous :

    - NSI Première Partie 4 Chapitre 1 Spécification et tests


<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/gJLFAS4gX2LGmAK" target="_blank">Téléchargement des ressources</a>
