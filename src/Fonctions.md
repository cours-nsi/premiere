<h1 style="text-align:center;">Chapitre 3 : les fonctions </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "fonctions.jpg" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "grace.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Jean_E._Sammet' target="_blank">Grace Murray Hopper </a>
est une informaticienne américaine et Rear admiral (lower half) de la marine américaine, née le 9 décembre 1906 à New York et morte le 1er janvier 1992 dans le comté d'Arlington. Pionnière de la programmation informatique, Grace Hopper a contribué au développement du langage COBOL et a joué un rôle essentiel dans le développement des premiers ordinateurs.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
La notion de fonction est abordée rapidement dans le cours. 
<br><br>
A l’instar de la notion de variable, la notion de fonction reflète deux modes de pensée différents en Mathématiques et en Informatique.
<br><br>
En Mathématiques, elle correspond à une relation entre deux ensembles, alors qu’en Informatique, elle fait réf ́erence à un procédé de calcul.
<br><br>
L’idée générale d’une fonction informatique est donc d’associer un nom à une séquence d’instructions particulière (on parle alors d’<span style='font-style:italic;'>encapsulation</span>) afin de pouvoir la réutiliser ultérieurement dans le programme, voire dans un autre programme.
<br><br>
Le but de ce chapitre est de présenter la notion de fonction et de manipuler des fonctions sur des exemples simples afin de pouvoir bien les appréhender.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un point de cours pour présenter la notion de fonction : déclaration, syntaxe ;</li>
<li>un notebook pour visualiser le fonctionnement d'une fonction lors de son appel et de son exécution ;</li>
<li>un point de cours sur les appels de fonctions et les portées des variables ;</li>
<li>un notebook d'entraînement pour comprendre et manipuler des fonctions ;</li>
<li>un QCM d'auto-évaluation ;</li>
<li>un notebook d'entraînement pour créer ses propres fonctions ;</li>
<li>un exercice complémentaire bilan (qui peut-être facultatif).</li>
</ol>
<br><br>


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Fonctions.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Booleens_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;display:none;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Accès via Capytale</h3>

Dans la zone Rechercher, taper les phrases ci-dessous :

    - NSI Première Partie 1 Chapitre 3 Fonctions introduction

    - NSI Première Partie 1 Chapitre 3 Fonctions partie 1

    - NSI Première Partie 1 Chapitre 3 Fonctions partie 2


<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/cD837jo4zn6BGxo" target="_blank">Téléchargement des ressources</a>
