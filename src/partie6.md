<h1 style="text-align:center;">Partie 6 : IHM et Web</h1>

<br>
<div style="text-align: center;"> 
<img src = "web.png" alt='image'>
</div>
<br>

Lors de la navigation sur le Web, les internautes interagissent avec leur machine par le biais des pages Web.
<br><br>
L’Interface Homme-Machine (IHM) repose sur la gestion d’événements associés à des éléments graphiques munis de méthodes algorithmiques.
<br><br>
La compréhension du dialogue client-serveur déjà abordé en classe de Seconde est consolidée, sur des exemples simples, en identifiant les requêtes du client, les calculs puis les réponses du serveur traitées par le client.
<br><br>
Il ne s’agit pas de décrire exhaustivement les différents éléments disponibles, ni de développer une expertise dans les langages qui permettent de mettre en oeuvre le dialogue tels que PHP ou JavaScript.

<br><hr><br>
Cette partie, entièrement consacrée au Web, abordera les points suivants :

<br>

1. Web statique :
    - langage HTML ;
    - langage CSS ;
    - différencier le fond et la forme.

<br>

2. Web interactif :
    - initiation au langage JavaScript ;
    - comprendre la philosophie de la programmation événementielle ;
    - comprendre que l'interactive se déroule côté client.

<br>

3. Web dynamique :
    - initiation au langage PHP ;
    - initiation aux formulaires d'une page Web ;
    - comprendre la philosophie des pages Web dynamiques ;
    - comprendre que la création des pages dynamique se déroule côté serveur.

<br>

Trois projets de création de sites Web jalonneront ces diverses notions.