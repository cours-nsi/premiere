<h1 style="text-align:center;">Chapitre 1 : réseaux </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                if (id_bouton == 'activite')
                {
                    elem_bouton.innerHTML = "Masquer l'activité d'introduction";
                }
                else
                {
                    elem_bouton.innerHTML = 'Masquer les exercices';
                }
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                if (id_bouton == 'activite')
                {
                    elem_bouton.innerHTML = "Afficher l'activité d'introduction";
                }
                else
                {
                    elem_bouton.innerHTML = 'Afficher les exercices';
                }
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "reseau2.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "radia.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Radia_Perlman' target="_blank">Radia Perlman</a>, connue sous le nom de &laquo; Mère d'Internet &raquo;, est une ingénieure en informatique qui a inventé le protocole de routage Spanning Tree Protocol (STP), protocole fondamental pour le fonctionnement des ponts en infrastructure de réseaux et a contribué au développement des protocoles réseau.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Le but de ce chapitre est de présenter quelques éléments clés d'un réseau :
<ul>
<li>histoire et évolution ;</li>
<li>éléments physiques ;</li>
<li>modèle TCP/IP ;</li>
<li>couches réseaux et protocoles ;</li>
<li>adresse IP et notation CIDR.</li>
</ul>
<br>
On propose le déroulé suivant :
<ol>
<li>une activité d'introduction sous la forme d'un questionnaire à compléter à partir de deux ressources vidéos ;</li>
<li>un point de cours bilan résumant toutes les notions abordées dans l'activité d'introduction ;</li>
<li>des exercices d'entraînement (exercices 1 à 5) pour compléter les notions vues dans le cours ;</li>
<li>un exercice complémentaire (exercice 6) liant requêtes client-serveur et programmation Python ;</li>
<li>des exercices (exercices 7 à 13) permettant de simuler un réseau à l'aide du logiciel <a href='https://www.lernsoftware-filius.de/Herunterladen'><code>Filius</code></a>.</li>
</ol>
<br><br> 


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='activite' onclick="affichage_iframe('activite','pdf-js-viewer-test2','Reseaux_cours_questionnaire.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher l'activité d'introduction</span>
 
<iframe id="pdf-js-viewer-test2" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Reseaux_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Reseaux_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/rzpBAzrZRWCctgr" target="_blank">Téléchargement des ressources</a>
