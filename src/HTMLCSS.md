<h1 style="text-align:center;">Chapitre 1 : Web statique</h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                if (id_bouton == 'activite')
                {
                    elem_bouton.innerHTML = "Masquer le TP1 (HTML)";
                }
                else
                {
                    if (id_bouton == 'projet')
                    {
                        elem_bouton.innerHTML = "Masquer le TP3 (projets)";
                    }
                    else
                    {
                        elem_bouton.innerHTML = 'Masquer le TP2 (CSS)';
                    }
                }
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                if (id_bouton == 'activite')
                {
                    elem_bouton.innerHTML = "Afficher le TP1 (HTML)";
                }
                else
                {
                    if (id_bouton == 'projet')
                    {
                        elem_bouton.innerHTML = "Afficher le TP3 (projets)";
                    }
                    else
                    {
                        elem_bouton.innerHTML = 'Afficher le TP2 (CSS)';
                    }
                }
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "web2.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "elisabeth.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Elizabeth_J._Feinler' target="_blank">Elizabeth J. Feinler</a>
 est une informaticienne américaine, pionnière de l'internet. Entre 1972 et 1989, elle est directrice du Network Information Center (NIC) au Stanford Research Institute (SRI International). En 1972, Feinler dirige la section de recherche de la bibliothèque du SRI quand l'ingénieur Doug Engelbart l'a recrute pour rejoindre l'Augmentation Research Center (ARC), un centre de recherche parrainé par l'Agence pour les projets de recherche avancée de défense (DARPA). Sa première tâche fut d'écrire un guide des ressources pour la première démonstration de l'ARPANET à la Conférence internationale Communication Computer. En 1974, elle est la principale investigatrice pour créer le nouveau Network Information Center (NIC) de l'ARPANET. 
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Le langage HTML (<span style='font-style:italic;'>HyperText Markup Language</span>) permet de décrire la structure et le contenu d’une page web (on parle alors de <span style='font-style:italic;'>langage descriptif</span>). Cette description est réalisée à l’aide d’un balisage <span style='font-style:italic;'>hypertexte</span> qui est ensuite
interprété par le navigateur pour afficher le contenu de la page web.
<br><br>
Le langage CSS (<span style='font-style:italic;'>Cascade Style Sheets</span>) permet de modifier les apparences par défaut des différentes balises HTML.
<br><br>
Le but de ce chapitre est de rappeler et approfondir, à travers deux TP à faire en autonomie, quelques éléments clés des langages HTML et CSS vus en classe de Seconde en SNT. Ces différents éléments sont ensuite utilisés pour créer un site Web statique.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un premier TP (TP1 HTML) présentant quelques éléments clés du langage HTML avec une fiche de synthèse à réaliser et à présenter. Les élèves utiliseront le document ressource <span style='font-style:italic;'>Introduction au HTML5</span> disponible en téléchargement ;</li>
<li>un deuxième TP (TP2 CSS) présentant quelques éléments clés du langage CSS avec une fiche de synthèse à réaliser et à présenter. Les élèves utiliseront les documents ressources <span style='font-style:italic;'>Introduction au HTML5</span> et <span style='font-style:italic;'>Introduction au CSS3</span> disponibles en téléchargement ;</li>
<li>un point de cours bilan reprenant l'essentiel de ce qui a été vu dans le deux TP précédents ;</li>
<li>un troisième TP (TP3 projets) permettant de mettre en oeuvre les notions vues précédemment en HTML et CSS à travers la réalisation d'un site Web statique.</li>
</ol>
<br><br>


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='activite' onclick="affichage_iframe('activite','pdf-js-viewer-test2','Web_statique_TP1.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le TP1 (HTML)</span>
 
<iframe id="pdf-js-viewer-test2" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Web_statique_TP2.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le TP2 (CSS)</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>


<br><br><br>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Web_statique_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="projet" name = 'exercice' onclick="affichage_iframe('projet','pdf-js-viewer-test5','Web_statique_TP3.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le TP3 (projets)</span>
 
<iframe id="pdf-js-viewer-test5" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/GPnJApnnPJb6jFx" target="_blank">Téléchargement des ressources</a>
