<h1 style="text-align:center;">Chapitre 2 : représentations des nombres entiers naturels </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "entiers.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "katherine.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Katherine_Johnson' target="_blank">Katherine Johnson</a>
 est une mathématicienne et informaticienne américaine. Réputée pour la fiabilité de ses calculs en navigation astronomique, elle conduit des travaux techniques à la NASA qui s'étalent sur des décennies. Elle a travaillé pour la NASA et a joué un rôle crucial dans les calculs orbitaux pour les missions spatiales, notamment pendant le programme Mercury.
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div>

<br>
En informatique, toutes les données, quelles qu'elles soient, sont représentées par une succession de 0 et de 1 appelé <span style='font-style:italic;'>mot binaire</span>.
<br><br>
La représentation d'une donnée sous forme de mot binaire est appelée <span style='font-style:italic;'>représentation binaire de la donnée</span>.
<br><br>
Le but de ce chapitre est de présenter et manipuler sur des exemples simples et pratiques les représentations binaires des nombres entiers naturels.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un point de cours expliquant pourquoi les données sont représentées par des mots binaires, puis présentant les représentations binaires et hexadécimales des nombres entiers naturels ;</li>
<li>des exercices d'entraînement pour manipuler les représentations binaires et hexadécimales des nombres entiers naturels ;</li>
<li>un notebook permettant d'implémenter deux fonctions de conversion : l'un de l'écriture décimale d'un entier naturel vers son écriture binaire, et l'autre de l'écriture binaire d'un entier naturel vers son écriture décimale.</li>
</ol>
<br><br>


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Codage_entiers_naturels_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Codage_entiers_naturels_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>


<h3>Accès via Capytale</h3>

Dans la zone Rechercher, taper la phrase ci-dessous :

    - NSI Première Partie 2 Chapitre 2 Codage entiers naturels


<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/sLFQ9trX5yxyTAF" target="_blank">Téléchargement des ressources</a>
