<h1 style="text-align:center;">Chapitre 2 : les composants </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "structure.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "lynn.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Lynn_Conway' target="_blank">Lynn Conway </a>
est une informaticienne et ingénieure en électronique. Elle a joué un rôle essentiel dans le développement des techniques de conception de circuits intégrés VLSI (Very Large Scale Integration).
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Vu de l’extérieur, un ordinateur est une machine de traitement de l’information : il est capable d’acquérir de l’information, de la stocker, de la transformer en effectuant des traitements quelconques, puis de la restituer sous une autre forme. L’information est bien sûr
prise au sens large : ce peut être du texte, des nombres, des sons, des images, etc.
<br><br>
Vu de l’intérieur, un ordinateur est un ensemble de composants électroniques qui coexistent et qui interagissent ensemble afin d’obtenir le résultat voulu par l’utilisateur.
<br><br>
Le but de ce second chapitre sur l'architecture des ordinateurs est de présenter quelques éléments généraux sur les composants d'un ordinateur.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un premier exercice à résoudre en s'aidant uniquement du cours (recherche documentaire) ;</li>
<li>un second exercice à résoudre en s'aidant éventuellement du Web.</li>
</ol>
<br><br>  


<div style="text-align: center;"> 
<img src = "cours.png">
</div>


<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Composants_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>


<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Composants_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>


</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>
<span style='font-style:italic;'>Il n'y a pas de ressources à télécharger.</span>