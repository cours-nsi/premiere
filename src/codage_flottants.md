<h1 style="text-align:center;">Chapitre 4 : représentation des nombres réels (flottants) </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "flottants.jpg" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src ="telley.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://en.wikipedia.org/wiki/Telle_Whitney' target="_blank">Telle Whitney</a>
 est une nformaticienne et ancienne présidente de l'Anita Borg Institute, qui encourage la participation des femmes dans l'informatique. Elle a cofondé la Grace Hopper Celebration of Women in Computing avec Anita Borg en 1994.
 Elle a obtenu un baccalauréat en informatique de l' Université de l'Utah en 1978 et un doctorat en informatique de Caltech en 1985. Elle a déménagé dans la Silicon Valley pour travailler dans l'industrie des puces, créant des puces et les logiciels qui les prennent en charge.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Le but de ce chapitre est de présenter et manipuler sur des exemples simples et pratiques les représentations binaires des nombres à virgules appelés <span style='font-style:italic;'>nombres flottants</span>.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un point de cours expliquant comment sont représentés en binaire les nombres flottants et quels sont les liens entre nombres flottants en Informatique et nombres réels en Mathématiques. On inclut également une partie optionnelle sur l'arithmétique flottante ;</li>
<li>des exercices d'entraînement pour manipuler les représentations binaires des nombres flottants sur quelques exemples simples.</li>
</ol>
<br><br>


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Codage_flottants_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Codage_flottants_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<br>
<span style='font-style:italic;'>Il n'y a pas de ressources à télécharger.</span>