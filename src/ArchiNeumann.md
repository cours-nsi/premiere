<h1 style="text-align:center;">Chapitre 3 : architecture de von Neumann </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "architecture.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "Sophie.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Sophie_Wilson' target="_blank">Sophie Wilson</a>
est une informaticienne britannique. Elle a conçu l'Acorn Micro-Computer, le premier d'une lignée d'ordinateurs personnels commercialisés par Acorn Computers Ltd, y compris son langage de programmation BBC BASIC.
Elle est également à l’origine de l'ensemble de la conception de l'architecture ARM, devenue l'architecture de processeur fondant le plus grand nombre de microprocesseurs vendus au monde, notamment dans les smartphones. Sophie Wilson a été nommée membre de la Royal Society (FRS, FREng) et de la Royal Academy of Engineering (RAEng) au Royaume-Uni
</p>
</div>
<br>
<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
La première description d’un ordinateur dont les programmes sont stockés dans sa mémoire a été élaborée en juin 1945 par le mathématicien John von Neumann (1903–1957) dans le cadre du projet EDVAC (Electronic Discrete Variable Automatic Computer).
<br><br>
Ce modèle, toujours utilisé de nos jours et connu sous le nom d’<span style='font-style:italic;'>architecture de von Neumann</span>, consiste à organiser l’unité centrale
de l’ordinateur en quatre éléments : la mémoire principale, l'unité de traitement, l'unité de commande et l'unité d'entrées/sorties.
<br><br>
Le but de ce chapitre est de présenter cette architecture en décrivant ces différents composants ainsi que leurs interactions.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un premier exercice à résoudre en s'aidant uniquement du cours (recherche documentaire) ;</li>
<li>un second exercice (réalisation d'un QCM) à faire en devoir maison (temps prévu : 1 semaine) ;</li>
<li>quelques groupes viennent présenter leur QCM à l'issue de ce travail à la maison.</li>
</ol>
<br><br> 

<div style="text-align: center;"> 
<img src = "cours.png">
</div>


<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Von_Neumann_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Von_Neumann_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>
<span style='font-style:italic;'>Il n'y a pas de ressources à télécharger.</span>