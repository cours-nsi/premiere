<h1 style="text-align:center;">Challenges</h1>

<br>
<div style="text-align: center;"> 
<img src = "challenges.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "claire.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Claire_Mathieu' target="_blank">Claire Mathieu</a>
 connue aussi sous le nom Claire Kenyon, née le 9 mars 1965, est une informaticienne et mathématicienne française, connue pour ses recherches sur les algorithmes d'approximation, les algorithmes en ligne, et la théorie des enchères. Elle travaille en tant que directrice de recherche au Centre National de la Recherche Scientifique (CNRS). Elle est chercheuse à l'Institut de recherche en informatique fondamentale.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
On propose ci-dessous, uniquement en téléchargement et au format notebook séquencé, divers challenges en algorithmique et en programmation Python et/ou Javascript.

<br><br>


<script type="text/javascript" src="utils.js"></script>


<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>


<br><hr><br>

<ol>
    <li><a href='https://nuage03.apps.education.fr/index.php/s/FqM98He5pbRzNix' target='_blank'>Art Expo</a> : aidez le galeriste à accéder aux douze tableaux les plus célèbres du monde avant l’inauguration de l’exposition Expo Art (d'après une activité de <a href='https://www.101computing.net/'>101computing.net</a>).
    </li>
    <br>
    <li><a href='https://nuage03.apps.education.fr/index.php/s/59oomymTCAKzebj' target='_blank'>Blackbeard’s Hidden Treasures</a> : découvrez les douze lieux où le célèbre pirate Barbe-Noire a caché son trésor (en anglais, d'après une activité de <a href='https://www.101computing.net/'>101computing.net</a>).
    </li>
    <br>
    <li><a href='https://nuage03.apps.education.fr/index.php/s/Gk3Z5AojfT8sTJR' target='_blank'>Blackbeard’s Treasure Map</a> : partez à la recherche du trésor du célèbre pirate Barbe-Noire (en anglais, d'après une activité de <a href='https://www.101computing.net/'>101computing.net</a>).
    </li>
    <br>
    <li><a href='https://nuage03.apps.education.fr/index.php/s/Abg3mQ4wqsJCFnE' target='_blank'>Connexion administrateur</a> : faites vous passer pour l'administrateur d'un système et connectez-vous sur son espace web dédié afin de récupérer son flag.
    </li>
    <br>
    <li><a href='https://nuage03.apps.education.fr/index.php/s/NtLAj7tzzDLctbQ' target='_blank'>Cryptage renforcé</a> : décodez le message secret caché à l'intérieur d'un dossier hautement crypté.
    </li>
    <br>
    <li><a href='https://nuage03.apps.education.fr/index.php/s/8eH4MaDe2TB9tgN' target='_blank'>Dossier protégé</a> : décodez un message caché à l'intérieur d'un dossier protégé par un mot de passe.
    </li>
    <br>
    <li><a href='https://nuage03.apps.education.fr/index.php/s/TGLHgHDkynsM2iD' target='_blank'>Espionnage (d'après Passe ton hack d'abord)</a> : exécutez une mission secrète à haut risque.
    </li>
    <br>
    <li><a href='https://nuage03.apps.education.fr/index.php/s/67fYgpjcFtSjdfp' target='_blank'>Football Premier League</a> : suivez d'au plus près le championnat de football de Premier League (en anglais, d'après une activité de <a href='https://www.101computing.net/'>101computing.net</a>).
    </li>
    <br>
    <li><a href='https://nuage03.apps.education.fr/index.php/s/sZrmcGtZ326KQbG' target='_blank'>Rois de France</a> : partez à la découverte de l'Histoire de France.
    </li>   
</ol>

<br><hr><br>

De nombreux autres challenges sont également disponibles sur ce <a href='https://cybersecurite.forge.apps.education.fr/cyber/3.Challenges/presentation/'>site</a> entièrement dédié à la cybersécurité. Ceux-ci sont regroupés en huit catégories :
<ul>
<li>algorithmique</li>
<li>cryptographie</li>
<li>OSINT</li>
<li>programmation Python/Scratch</li>
<li>réseaux</li>
<li>stéganographie</li>
<li>web (client)</li>
<li>web (serveur)</li>
</ul>