<h1 style="text-align:center;">Chapitre 4 : tris de tableaux</h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "tri.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "adele.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Adele_Goldberg' target="_blank">Adele Goldberg</a>
 est une informaticienne américaine. Elle a été l'une des co-développeuses du langage de programmation Smalltalk -80 et de divers concepts liés à la programmation orientée objet alors qu'elle était chercheuse au Xerox Palo Alto Research Center (PARC), dans les années 1970.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
La recherche d’un élément dans un tableau est plus rapide si celui-ci est trié. Prenons l’exemple d’une bibliothèque : même s’il est long
et fatiguant de ranger les livres une fois pour toutes (par exemple dans l’ordre alphabétique), cela vaut mieux que de les laisser en vrac
et devoir ainsi parcourir des kilomètres de rayonnage à chaque fois que l’on cherche un livre.
<br><br>
Ceci amène tout naturellement au problème suivant : <span style='font-style:italic;'>comment trier un tableau de manière la plus rapide possible pour que celui-ci soit ordonné ?</span>
<br><br>
Le but de ce chapitre est de présenter ici deux méthodes classiques de tri : le <span style='font-style:italic;'>tri par sélection</span> et le <span style='font-style:italic;'>tri par insertion</span>.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un point de cours (paragraphes 1 et 2) pour introduire la notion de tri de tableaux. On présente en particulier un premier tri : le <span style='font-style:italic;'>tri par permutation</span> ;</li>
<li>un notebook introduisant le tri par sélection ;</li>
<li>un point de cours bilan sur le tri par sélection (paragraphe 3) ;</li>
<li>des exercices d'entraînement (exercices 1 et 2) sur le tri par sélection ;</li>
<li>un notebook introduisant le tri par insertion ;</li>
<li>un point de cours bilan sur le tri par insertion (paragraphe 4);</li>
<li>des exercices d'entraînement (exercices 3 à 5) sur le tri par insertion ;</li>
<li>un exercice complémentaire facultatif sur le tri par dénombrement.</li>
</ol>
<br><br> 


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Tris_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Tris_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>


<h3>Accès via Capytale</h3>

Dans la zone Rechercher, taper les phrases ci-dessous :

    - NSI Première Partie 4 Chapitre 4 Tri sélection

    - NSI Première Partie 4 Chapitre 4 Tri insertion


<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/5CpfbSCf2Ss4K5w" target="_blank">Téléchargement des ressources</a>
