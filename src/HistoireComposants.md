<h1 style="text-align:center;">Chapitre 1 : historique et évolution des ordinateurs </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "histoire.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "Karen.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Karen_Sp%C3%A4rck_Jones' target="_blank">Karen Spärck Jones</a> née le 26 août 1935 à Huddersfield et décédée le 4 avril 2007 est une scientifique britannique, chercheuse en informatique. Ses travaux concernent le domaine de l'intelligence artificielle, et principalement le traitement automatique du langage naturel et la recherche d'information.
Son article publié en 1964 Classification de la synonymie et sémantique est désormais considéré comme un document fondamental dans le domaine du traitement du langage naturel1.
En 1972, elle publie dans Journal of Documentation un article qui donne les bases des moteurs de recherche en combinant les statistiques et la linguistique et qui indique de quelles manières les ordinateurs interprètent les relations entre les mots
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Depuis des milliers d'années, l'homme a créé, fabriqué et utilisé des outils pour l'aider à calculer.
Pourquoi les ordinateurs ? De quand datent-ils ? Comment ont-ils évolués ?
<br><br>
Le but de ce premier chapitre sur l'architecture des ordinateurs est de répondre à ces premières questions générales.
<br><br>
On propose le déroulé suivant :
<ol>
<li>cinq exercices et un exercice de construction d'une frise chronologique à réaliser en effectuant des recherches sur le Web ;</li>
<li>un point de cours présentant l'essentiel à retenir.</li>
</ol>
<br><br> 


<div style="text-align: center;"> 
<img src = "cours.png">
</div>


<div class='centre'>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','HistoireArchi_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','HistoireArchi_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>
<span style='font-style:italic;'>Il n'y a pas de ressources à télécharger.</span>