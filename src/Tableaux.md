<h1 style="text-align:center;">Chapitre 3 : les tableaux</h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "tableau.jpg" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "radhika.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://en.wikipedia.org/wiki/Radhika_Nagpal' target="_blank">Radhika Nagpal</a>
 est une informaticienne et chercheuse indo-américaine dans les domaines des systèmes informatiques auto-organisés, de la robotique d'inspiration biologique et des systèmes multi-agents biologiques . Elle est professeur Augustine en ingénierie dans les départements d'ingénierie mécanique et aérospatiale et d'informatique de l'Université de Princeton . 
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Dans le chapitre 6 de la partie 1 dédiée à la programmation Python, nous avons introduit la notion de listes (type <code>list</code>) qui permet d'implémenter la structure de données <span style='font-style:italic;'>tableau</span>.
<br><br>
Le but de ce chapitre est d'étudier cette structure <span style='font-style:italic;'>tableau</span> d'un point de vue algorithmique. On ne s'intéresse ici qu'à des tableaux <span style='font-style:italic;'>statiques</span>.
<br><br>
On propose le déroulé suivant :
<ol>
<li>lecture interactive du cours et résolution des exercices qui y sont intégrés. On aborde en particulier les points suivants :
<ul>
<li>pourquoi des tableaux ?</li>
<li>longueur et indexation d'un tableau ;</li>
<li>parcours séquentiel d'un tableau ;</li>
<li>recherche d'éléments dans un tableau.</li>
</ul></li>
<li>des exercices d'entraînement pour manipuler les tableaux d'un point de vue algorithmique (on ne s'interdit pas ici d'implémenter les algorithmes écrits en Python).</li>
</ol>
<br><br> 


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Tableaux_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Tableaux_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>
<span style='font-style:italic;'>Il n'y a pas de ressources à télécharger.</span>
