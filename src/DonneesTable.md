<h1 style="text-align:center;">Chapitre 10 : données en table</h1>

<br>
<div style="text-align: center;"> 
<img src = "table.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "soeur.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Mary_Kenneth_Keller' target="_blank">Mary Kenneth Keller</a>, sœur de la charité de la Bienheureuse Vierge Marie, B.V.M. (1913 – 1985), est une religieuse catholique américaine qui fut enseignante et pionnière dans les sciences informatiques. Elle est la première femme à obtenir un doctorat en informatique aux États-Unis. Elle est pionnière de l'intelligence artificielle, a cofondé l'AI Lab à l'Université Stanford et a contribué à la création du langage de programmation BASIC.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Au chapitre 8, nous avons appris à manipuler les fichiers de type <span style='font-style:italic;'>texte brut</span> (extension <code>.txt</code>).
<br><br>
Le but de ce chapitre est d'apprendre à manipuler un autre type de fichier : les fichiers csv.
<br><br>On propose le déroulé suivant :
<ol>
<li>un notebook d'introduction à la manipulation des fichiers csv, faisant office de cours ;</li>
<li>un notebook d'approfondissement et d'entraînement.</li>
</ol>

<br><br>

<div style="text-align: center;"> 
<img src = "cours.png">
</div> 
<br>
Le cours est inclus dans le TP ci-dessous.

<br><br>
<script type="text/javascript" src="utils.js"></script>


<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Accès via Capytale </h3>
Dans la zone Rechercher, taper les phrases ci-dessous :

    - NSI Première Partie 1 Chapitre 10 Données en table 1/2

    - NSI Première Partie 1 Chapitre 10 Données en table 2/2


<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/xGgoH47QLw3Rxj3" target="_blank">Téléchargement des ressources</a>

