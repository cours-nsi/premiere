<h1 style="text-align:center;">Chapitre 7 : lignes de commandes </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le TP1 (Terminus)';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer le TP2 (Shell)';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le TP1 (Terminus)';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher le TP2 (Shell)';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "jean.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "jane.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Jean_Bartik' target="_blank">Jean Bartik</a>
est une informaticienne américaine, membre du groupe des six programmeuses de l'ENIAC. Elle est intronisée en 1997 au Women in Technology International Hall of Fame. En 2008, elle est une des trois lauréates du prix Fellow du Computer History Museum avec Bob Metcalfe et Linus Torvalds.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Le but de ce chapitre est de présenter, à travers deux TP à faire en autonomie, quelques instructions en ligne de commande.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un premier TP présentant quelques commandes de base (TP1 Terminus) sous la forme d'un jeu ;</li>
<li>un second TP présentant l'arborescence de fichiers et la gestion des droits et permissions d'accès aux fichiers (TP2 Shell).</li>
</ol>
<br><br> 

<div style="text-align: center;"> 
<img src = "cours.png">
</div>


<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','TP1-Terminus.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le TP1 (Terminus)</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','TP2-Shell.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le TP2 (Shell)</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>
<span style='font-style:italic;'>Il n'y a pas de ressources à télécharger.</span>