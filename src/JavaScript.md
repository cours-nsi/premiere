<h1 style="text-align:center;">Chapitre 2 : Web interactif</h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                if (id_bouton == 'activite')
                {
                    elem_bouton.innerHTML = "Masquer le TP1 (introduction)";
                }
                else
                {
                    if (id_bouton == 'projet')
                    {
                        elem_bouton.innerHTML = "Masquer le TP3 (projets)";
                    }
                    else
                    {
                        elem_bouton.innerHTML = 'Masquer le TP2 (manipulation)';
                    }
                }
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                if (id_bouton == 'activite')
                {
                    elem_bouton.innerHTML = "Afficher le TP1 (introduction)";
                }
                else
                {
                    if (id_bouton == 'projet')
                    {
                        elem_bouton.innerHTML = "Afficher le TP3 (projets)";
                    }
                    else
                    {
                        elem_bouton.innerHTML = 'Afficher le TP2 (manipulation)';
                    }
                }
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "web1.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "Roberta.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Roberta_Williams' target="_blank">Roberta Williams</a>
 (née le 16 février 1953) est connue pour être à l'origine du jeu vidéo Mystery House, le premier jeu d'aventure graphique, dont le succès fut tel qu'avec son mari Ken Williams, elle créa On-Line Systems en 1980 qui devint en 1982, Sierra On-Line. On lui doit de nombreux autres classiques du jeu d'aventure. La créatrice de jeu-vidéo américaine a mis au point Mystery House en 1980, avec son mari Ken Williams. Autrefois en mode texte uniquement, le monde du jeu-vidéo est alors propulsé dans une nouvelle dimension.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Le langage JavaScript est un langage de programmation interprété permettant de créer des sites Web <span style='font-style:italic;'>interactifs</span>, c'est-à-dire des sites Web qui puissent afficher du contenu en fonction d'événements réalisés côté client (clic de souris, survol d'une zone à l'aide de la souris, etc.).
<br><br>
Le but de ce chapitre est double :
<ul>
<li>présenter et manipuler, à travers deux TP à faire en autonomie, quelques éléments clés du langage JavaScript ;</li>
<li>comprendre les notions générales liées à la programmation événementielle.</li>
</ul> <br>
On propose le déroulé suivant :
<ol>
<li>un premier TP (TP1 introduction) présentant quelques éléments clés du langage JavaScript avec une fiche de synthèse à réaliser et à présenter. Les élèves utiliseront le document ressource <span style='font-style:italic;'>Introduction au JavaScript</span> disponible en téléchargement ;</li>
<li>un deuxième TP (TP2 manipulation) pour manipuler et approfondir les notions vues au TP précédent avec une fiche de synthèse à réaliser et à présenter. Les élèves utiliseront les documents ressources <span style='font-style:italic;'>Introduction au HTML5</span>, <span style='font-style:italic;'>Introduction au CSS3</span> et <span style='font-style:italic;'>Introduction au JavaScript</span> disponibles en téléchargement ;</li>
<li>un point de cours bilan reprenant l'essentiel de ce qui a été vu dans le deux TP précédents ;</li>
<li>un troisième TP (TP3 projets) permettant de mettre en oeuvre les notions vues précédemment en JavaScript à travers la réalisation d'un site Web interactif.</li>
</ol>
<br><br>


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='activite' onclick="affichage_iframe('activite','pdf-js-viewer-test2','Web_interactif_TP1.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le TP1 (introduction)</span>
 
<iframe id="pdf-js-viewer-test2" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Web_interactif_TP2.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le TP2 (manipulation)</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>


<br><br><br>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Web_interactif_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="projet" name = 'exercice' onclick="affichage_iframe('projet','pdf-js-viewer-test5','Web_interactif_TP3.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le TP3 (projets)</span>
 
<iframe id="pdf-js-viewer-test5" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/x5GKtXmktBFzzmC" target="_blank">Téléchargement des ressources</a>
