<h1 style="text-align:center;">Chapitre 4 : portes logiques </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "portes.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "anca.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='http://people.eecs.berkeley.edu/~anca/index.html' target="_blank">Anca Dragan</a>
 est professeure associée au département EECS de l'UC Berkeley . Son objectif est de permettre aux robots de travailler avec, autour et à l'appui des gens. Elle dirige le laboratoire InterACT , travaillant sur les algorithmes d'interaction homme-robot, des algorithmes qui vont au-delà de la fonction du robot de manière isolée et génèrent un comportement de robot qui se coordonne bien avec les gens. 
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Au chapitre 3, nous nous sommes intéressés aux composants des ordinateurs dans l'architecture de von Neumann, ainsi qu'à leurs interactions.
<br><br>
Tous ces composants sont en fait élaborés à partir de circuits électroniques plus ou moins complexes, eux-mêmes construits à partir d’un seul et même type de composant : le <span style='font-style:italic;'>transistor</span>.
<br><br>
Ces différents circuits électroniques sont classés en différentes catégories : les <span style='font-style:italic;'>circuits logiques élémentaires</span>, également appelés <span style='font-style:italic;'>portes</span>, les <span style='font-style:italic;'>circuits combinatoires</span> et les
<span style='font-style:italic;'>circuits séquentiels</span>.
<br><br>
Le but de ce chapitre est de présenter quelques exemples de constructions de tels circuits.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un point de cours (paragraphe 1) présentant le transistor ;</li>
<li>un premier exercice (exercice 1) permettant de prendre en main le logiciel <a href='http://www.cburch.com/logisim/index.html'><code>Logisim</code></a> et de réaliser les trois portes <code>NON</code>, <code>ET</code> et <code>OU</code> ;</li>
<li>un point de cours (paragraphe 2) pour faire un bilan écrit des trois portes réalisées dans l'exercice 1 ;</li>
<li>trois exercices (exercices 2 à 4) permettant de réaliser à l'aide du logiciel <a href='http://www.cburch.com/logisim/index.html'><code>Logisim</code></a> les trois portes <code>NON ET</code>, <code>NON OU</code> et <code>OU EXCLUSIF</code> ;</li>
<li>un point de cours (paragraphe 3) présentant deux exemples simples de circuits combinatoires : le multiplexeur et l'additionneur 1 bit ;</li>
<li>un exercice (exercice 5) permettant de réaliser un additionneur multi-bits à l'aide du logiciel <a href='http://www.cburch.com/logisim/index.html'><code>Logisim</code></a> ;</li>
<li>un exercice (exercice 6) traitant un circuit combinatoire permettant de réaliser la fonction booléenne <code>EQUIVALENT A</code>.</li>
</ol>
<br><br> 

<div style="text-align: center;"> 
<img src = "cours.png">
</div>


<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Portes_logiques_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Portes_logiques_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>
<span style='font-style:italic;'>Il n'y a pas de ressources à télécharger.</span>