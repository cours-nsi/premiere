<h1 style="text-align:center;">Chapitre 2 : preuve et complexité d’algorithmes</h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "complexite.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "jwing.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Jeannette_Wing' target="_blank">Jeannette Wing</a>
 est une professeure d'informatique à l'Université Carnegie-Mellon (sur une chaire intitulée professeur du président). Elle est directrice du département d'informatique. Ses domaines d'intérêt sont les systèmes de spécification et de vérification, concurrents et répartis, ainsi que les langages de programmation.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Lorsque l’on écrit un algorithme, il faut pouvoir s’assurer qu’il s’exécute correctement dans toutes les situations. Un simple jeu de
tests ne suffit pas à assurer que le programme se termine et produit le résultat attendu à tous les coups. Dans certaines applications
industrielles, comme par exemple les programmes contrôlant la conduite des lignes de mêtro automatiques, il est même vital de démontrer qu’un programme est correct.
<br><br>
Par ailleurs, si plusieurs programmes permettent de répondre à un même problème, il faut être capable de les comparer afin de savoir lequel (ou lesquels) sont les plus efficaces.
<br><br>
Le but de ce second chapitre dédié à l'algorithmique est de présenter les méthodes mises en oeuvre pour répondre à ces deux problématiques qui sont les <span style='font-style:italic;'>preuves d'algorithmes</span> et la <span style='font-style:italic;'>complexité</span>.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un point de cours (paragraphe 1) présentant la notion de preuve algorithmique. On introduit en particulier les notions de terminaison avec le variant de boucle et de correction partielle avec l'invariant de boucle, et on illustre ces deux notions sur des exemples simples ;</li>
<li>des exercices d'entraînement (exercices 1 à 4) pour manipuler les variants et invariants de boucle ;</li>
<li>un point de cours (paragraphe 2) pour introduire sur un exemple simple la notion de complexité algorithmique. On définit en particulier une échelle de comparaison et on présente quelques résultats sur les temps de calcul ;</li>
<li>un exercice d'entraînement (exercice 5) pour manipuler le concept de complexité.</li>
</ol>
<br><br>


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','PreuveComplexite_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','PreuveComplexite_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>
<span style='font-style:italic;'>Il n'y a pas de ressources à télécharger.</span>
