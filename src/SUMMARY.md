# Summary

[Introduction](introduction.md)
[Prise en main](prise.md)
[Programme](programme.md)

- [Partie 1 : programmation Python](partie1.md)
    - [Initiation à Python](PriseEnMainPython.md)
    - [Variables et types](Variables_types.md)
    - [Les fonctions](Fonctions.md)
    - [Conditions et embranchements](ConditionsEmbranchements.md)
    - [Boucles](Boucles.md)
    - [Listes](Listes.md)
    - [Tuples](Tuples.md)
    - [Textes et fichiers](TextesFichiers.md)
    - [Dictionnaires](Dictionnaires.md)
    - [Données en table](DonneesTable.md)
    - [Projets](ProjetsProgrammation.md)
    - [Challenges](ChallengesProgrammation.md)

- [Partie 2 : représentation des données](partie2.md) 
    - [Booléens](Booleens.md)
    - [Représentation des entiers naturels](codage_entiers.md)
    - [Représentation des entiers signés](codage_signes.md)
    - [Représentation des flottants](codage_flottants.md)
    - [Représentation des caractères](codage_caracteres.md)




- [Partie 3 : architecture des ordinateurs](partie3.md) 
    - [Histoire](HistoireComposants.md)
    - [Composants](HistoireComposants_2.md)
    - [Architecture de von Neumann](ArchiNeumann.md)
    - [Portes logiques](PortesLogiques.md)
    - [Langage assembleur](LangageAssembleur.md)
    - [Systèmes d'exploitation](SystemesExploitation.md)
    - [Lignes de commande](LignesCommande.md)


- [Partie 4 : algorithmique avancée](partie4.md) 
    - [Introduction à l'algorithmique](IntroAlgo_cours.md)
    - [Preuve et complexité d'algorithmes](PreuveComplexite.md)
    - [Tableaux](Tableaux.md)
    - [Tris](Tris.md)
    - [Recherche dichotomique](RechercheDichotomique.md)
    - [k plus proches voisins](kNN.md)
    - [Algorithmes gloutons](AlgoGloutons.md)

- [Partie 5 : réseaux](partie5.md)
    - [Réseaux](Reseaux.md)
    - [Bit alterné](BitAlterne.md)

- [Partie 6 : IHM et Web](partie6.md)
    - [Web statique](HTMLCSS.md)
    - [Web interactif](JavaScript.md)
    - [Web dynamique](PHP.md)
    
[Bibliographie et sitographie](partie7.md)

[Remerciements et crédits](credits.md)