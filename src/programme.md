<h1 style="text-align:center;">Programme de 1 NSI</h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le programme';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le programme';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "programme.jpg" alt='image'>
</div> 
<br>

L’enseignement de spécialité de numérique et sciences informatiques du cycle terminal de la
voie générale vise l’appropriation des fondements de l’informatique pour préparer les élèves
à une poursuite d’études dans l’enseignement supérieur, en les formant à la pratique d’une
démarche scientifique et en développant leur appétence pour des activités de recherche.

L’objectif de cet enseignement, non professionnalisant, est l’appropriation des concepts et
des méthodes qui fondent l’informatique, dans ses dimensions scientifiques et techniques.
Cet enseignement s’appuie sur l’universalité de quatre concepts fondamentaux et la variété
de leurs interactions :

1. les données, qui représentent sous une forme numérique unifiée des informations
très diverses : textes, images, sons, mesures physiques, sommes d’argent, etc ;
2. les algorithmes, qui spécifient de façon abstraite et précise des traitements à
effectuer sur les données à partir d’opérations élémentaires ;
3. les langages, qui permettent de traduire les algorithmes abstraits en programmes
textuels ou graphiques de façon à ce qu’ils soient exécutables par les machines ;
4. les machines, et leurs systèmes d’exploitation, qui permettent d’exécuter des
programmes en enchaînant un grand nombre d’instructions simples, assurant la
persistance des données par leur stockage, et de gérer les communications. On y
inclut les objets connectés et les réseaux.

<br>
  Cliquer <a href='https://eduscol.education.fr/document/30007/download' target='_blank'>ici</a> pour télécharger le programme de première NSI.</p>
<br>
<br>

<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','bo.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le programme</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Booleens_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;display:none;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>