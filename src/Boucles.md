<h1 style="text-align:center;">Chapitre 5 : boucles </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "boucle.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "Rediet.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://en.wikipedia.org/wiki/Rediet_Abebe' target="_blank">Rediet Abebe</a>
est une informaticienne et chercheuse en intelligence artificielle.. Elle travaille sur des questions d'équité, de justice et d'inclusion dans les systèmes d'apprentissage automatique.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Les <span style='font-style:italic;'>structures de boucle</span> sont des structures algorithmiques qui permettent de répéter des instructions.
<br><br>
Le nombre de répétitions que l’on souhaite réaliser peut être prédéfini à l’avance. On parle alors de <span style='font-style:italic;'>boucle itérative</span>, ou <span style='font-style:italic;'>boucle bornée</span>.
<br><br>
Mais, le nombre de répétitions que l’on va réaliser peut aussi être inconnu, car dépendant d’une condition qui doit être vérifiée. Dans ce
cas, on parle alors de <span style='font-style:italic;'>boucle conditionnelle</span>, ou <span style='font-style:italic;'>boucle non bornée</span>.
<br><br>
Le but de ce chapitre est d'introduire et manipuler ces deux notions de boucle.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un point de cours pour présenter les boucles itératives ;</li>
<li>un notebook d'entraînement sur les boucles itératives ;</li>
<li>un QCM d'auto-évaluation ;
<li>un point de cours pour présenter les boucles conditionnelles ;</li>
<li>un notebook d'entraînement sur les boucles conditionnelles ;</li>
<li>un QCM d'auto-évaluation ;</li>
<li>des exercices complémentaires facultatifs utilisant la bibliothèque <code>turtle</code>.</li>
</ol>
<br><br>


<div style="text-align: center;"> 
<img src = "cours.png">
</div>

<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Boucles.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Booleens_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;display:none;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>



<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Accès via Capytale </h3>

Dans la zone Rechercher, taper les phrases ci-dessous :

    - NSI Première Partie 1 Chapitre 5 Boucles itératives

    - NSI Première Partie 1 Chapitre 5 Boucles conditionnelles

<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/wNAbrCQDDkCR7MN" target="_blank">Téléchargement des ressources</a>

