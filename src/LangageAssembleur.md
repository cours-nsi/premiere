<h1 style="text-align:center;">Chapitre 5 : langage assembleur </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "assembleur.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "frances.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Frances_Allen' target="_blank">Frances Allen</a> a essentiellement travaillé sur l'optimisation des compilateurs. Elle a obtenu des résultats importants sur les compilateurs, l'optimisation de code et le calcul parallèle. Elle est la première femme à obtenir le titre d'IBM Fellow, et la première à recevoir le prix Turing.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
En informatique, les langages de programmation sont catégorisés en trois grandes catégories :
<ul>
<li>le <span style='font-style:italic;'>langage machine</span> ;</li>
<li>les <span style='font-style:italic;'>langages assembleurs</span> ;</li>
<li>les <span style='font-style:italic;'>langages de haut niveau</span>.</li>
</ul>
<br>
Le but de ce chapitre est d'expliquer les différences fondamentales entre ces trois grandes catégories et de faire une initiation au langage assembleur avec un jeu d'instructions élémentaires. On montre également plus en détail les interactions entre les différents composants du processeur et la mémoire principale lorsqu'un programme s'exécute.
<br><br>
On propose le déroulé suivant :
<ol>
<li>lecture interactive du cours et résolution des exercices qui y sont intégrés. On aborde en particulier les points suivants :
<ul>
<li>pourquoi un langage assembleur ?</li>
<li>quelques rappels et compléments sur l'architecture de von Neumann ;</li>
<li>une initiation au langage assembleur avec un jeu d'instructions très simple et un exemple pas à pas.</li>
</ul></li>
<li>des exercices d'entraînement (exercices 1 à 7) pour manipuler le langage assembleur vu en cours ;</li>
<li>un exercice complémentaire (exercice 8) utilisant plusieurs accumulateurs et un jeu d'instructions enrichi.</li>
</ol>
<br><br> 

<div style="text-align: center;"> 
<img src = "cours.png">
</div>


<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Langage_assembleur_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Langage_assembleur_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>
<span style='font-style:italic;'>Il n'y a pas de ressources à télécharger.</span>