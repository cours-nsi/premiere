<h1 style="text-align:center;">Chapitre 3 : Web dynamique</h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                if (id_bouton == 'activite')
                {
                    elem_bouton.innerHTML = "Masquer le TP1 (introduction)";
                }
                else
                {
                    if (id_bouton == 'projet')
                    {
                        elem_bouton.innerHTML = "Masquer le TP3 (projets)";
                    }
                    else
                    {
                        elem_bouton.innerHTML = 'Masquer le TP2 (projets)';
                    }
                }
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                if (id_bouton == 'activite')
                {
                    elem_bouton.innerHTML = "Afficher le TP1 (introduction)";
                }
                else
                {
                    if (id_bouton == 'projet')
                    {
                        elem_bouton.innerHTML = "Afficher le TP3 (projets)";
                    }
                    else
                    {
                        elem_bouton.innerHTML = 'Afficher le TP2 (projets)';
                    }
                }
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "web3.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "susan.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://en.wikipedia.org/wiki/Susan_J._Eggers' target="_blank">Susan J. Eggers</a>
 est une informaticienne américaine connue pour ses recherches sur l'architecture informatique et les compilateurs.
  Elle est surtout connue pour son travail fondamental dans le développement et la commercialisation de processeurs multithread simultanés (SMT), l'une des avancées les plus importantes de l'architecture informatique au cours des 30 dernières années. Au milieu des années 1990, la loi de Moore battait son plein et, alors que les ingénieurs informaticiens trouvaient des moyens d'installer jusqu'à 1 milliard de transistors sur une puce informatique, l'augmentation de la logique et de la mémoire à elle seule n'a pas entraîné de gains de performances significatifs. Eggers faisait partie de ceux qui ont soutenu que l'augmentation du parallélisme, ou la capacité d'un ordinateur à effectuer de nombreux calculs ou processus simultanément, était le meilleur moyen de réaliser des gains de performances.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Le langage PHP est un langage de programmation impératif orienté objet permettant de créer des sites Web <span style='font-style:italic;'>dynamique</span>, c'est-à-dire des sites Web qui puissent afficher du contenu changeant sans l'intervention d'un développeur pour en modifier le code. Ce langage s'exécute sur un serveur et est souvent utilisé pour générer des pages Web à partir de données reçues via un formulaire (page de connexion, achat en ligne, etc.)
<br><br>
Le but de ce chapitre est de comprendre et manipuler quelques notions liées aux formulaires HTML et aux scripts PHP pour générer des pages dynamiques.<br><br>
On propose le déroulé suivant :
<ol>
<li>un premier TP (TP1 introduction) présentant quelques notions sur les formulaires HTML et sur les script PHP avec une fiche de synthèse à réaliser et à présenter. Les élèves utiliseront les documents ressources <span style='font-style:italic;'>Introduction au PHP</span> et <span style='font-style:italic;'>Introduction aux formulaires HTML5</span> disponibles en téléchargement ;</li>
<li>un point de cours bilan reprenant l'essentiel de ce qui a été vu dans le le TP précédent ;</li>
<li>un second TP (TP2 projets) permettant de mettre en oeuvre les notions vues précédemment sur les formulaires et le PHP à travers la réalisation d'un site Web dynamique.</li>
</ol>
<br><br> 


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='activite' onclick="affichage_iframe('activite','pdf-js-viewer-test2','Web_dynamique_TP1.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le TP1 (introduction)</span>
 
<iframe id="pdf-js-viewer-test2" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>


<br><br><br>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Web_dynamique_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Web_dynamique_TP2.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le TP2 (projets)</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>


</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/DsbiBs2X9iKGAeJ" target="_blank">Téléchargement des ressources</a>
