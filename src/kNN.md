<h1 style="text-align:center;">Chapitre 6 : les k plus proches voisins</h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le TP (kNN)';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le TP (kNN)';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "voisins.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "ida.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Ida_Rhodes' target="_blank">Ida Rhodes</a>
a été une pionnière de l'analyse de systèmes de programmation, et avec Betty Holberton a conçu le langage de programmation C-10 au début des années 1950s qui serait mis en fonctionnemment dans UNIVAC I3. Elle a aussi conçu l'ordinateur original utilisé par l'Administration de la sécurité sociale américaine. En 1949 le Département du Commerce des États-Unis lui a remi la Médaille d'or pour ses contributions scientifiques et ses applications en électronique digital.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
L'<span style='font-style:italic;'>algorithme des k plus proches voisins</span>, ou <span style='font-style:italic;'>algorithme kNN</span>, est un exemple d'algorithme d'apprentissage automatique (en anglais : <span style='font-style:italic;'>machine learning</span>).
<br><br>
Le but de ce chapitre est de présenter, à travers un TP en autonomie, cet algorithme sur quelques exemples simples.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un TP présentant le principe général de l'algorithme des k plus proches voisins (TP kNN).</li>
<li>un notebook d'entraînement pour implémenter l'algorithme des k plus proches voisins dans un cas simple.</li>
</ol>
<br><br> 


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','kNN_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le TP (kNN)</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','AlgoGloutons_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;display:none;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Accès via Capytale</h3>

Dans la zone Rechercher, taper la phrase ci-dessous :

    - NSI Première Partie 4 Chapitre 6 kNN


<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/cmbLFLnYm4pMgyG" target="_blank">Téléchargement des ressources</a>
