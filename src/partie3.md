<h1 style="text-align:center;">Partie 3 : architecture des ordinateurs</h1>

<br>
<div style="text-align: center;"> 
<img src = "archi.png" alt='image'>
</div>
<br>

Exprimer un algorithme dans un langage de programmation a pour but de le rendre exécutable par une machine dans un contexte donné.
<br><br>
La découverte de l’architecture des machines et de leur système d’exploitation constitue une étape importante vers cette transcription, puisque les circuits électroniques sont au coeur de toutes les machines informatiques et que les systèmes d’exploitation gèrent et optimisent l’ensemble des fonctions de la machine, de l’exécution des programmes aux entrées-sorties et à la gestion d’énergie.

<br><hr><br>
Cette partie, entièrement consacrée à l'architecture des ordinateurs, abordera les points suivants :

<br>

1. Historique et évolution des ordinateurs :
    - pourquoi les ordinateurs ? ;
    - les différentes générations d'ordinateurs ;
    - les transistors, circuits intégrés et microprocesseurs ;
    - les langages informatiques ;
    - les systèmes d'exploitations.

<br>

2. Composants :
    - schéma fonctionnel d'un ordinateur ;
    - carte mère ;
    - types de mémoire ;
    - périphériques ;
    - démarrage de l'ordinateur : BIOS, UEFI et OS.

<br>

3. Architecture de von Neumann :
    - description du processeur : unités fonctionnelles, unité de commande, unité de traitement et horloge ;
    - mémoire principale ;
    - unité d'entrées/sorties ;
    - bus de communication.

<br>

4. Portes logiques :
    - description du transistor ;
    - portes logiques élémentaires ;
    - circuits combinatoires.

<br>

5. Langage assembleur :
    - différentes catégories de langage de programmation ;
    - liaison mémoire/processeur ;
    - initiation au langage assembleur.


<br>

6. Systèmes d'exploitation :
    - exemples de systèmes d'exploitation ;
    - systèmes d'exloitation libres et propriétaires ;
    - fonctions d'un système d'exploitation.

<br>

7. Lignes de commande :
    - découverte des commandes de base ;
    - arborescence de fichiers ;
    - gestion des droits et permissions d'accès aux fichiers.

