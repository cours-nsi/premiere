<h1 style="text-align:center;">Chapitre 5 : recherche dichotomique</h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "binairy.jpg" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "joan.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Joan_Clarke' target="_blank">Joan Clarke</a>
est une cryptologue britannique. Elle est principalement connue pour sa participation au décryptage de la machine Enigma qui codait les communications chiffrées du Troisième Reich. Elle est recrutée en juin 1940 par son ancien superviseur académique Gordon Welchman pour travailler au Government Code and Cypher School (GC&CS), à Bletchley Park, au sein de la Hutte 8, chargée du décryptage des codes de la Kriegsmarine, où elle est par ailleurs la seule femme.
Elle devient rapidement l'une des meilleures parmi les pratiquants du banburismus, une méthode de cryptanalyse développée par Alan Turing, dont elle est l'une des plus proches amies et très brièvement la fiancée.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
On a vu précédemment comment rechercher un élément dans un tableau. Si celui-ci n'est pas trié, cette recherche nous oblige à parcourir l'intégralité du tableau, que l'élément soit présent ou non. En revanche, si celui-ci est trié, la recherche peut être beaucoup plus efficace comme nous allons le voir.
<br><br>
La <span style='font-style:italic;'>recherche dichotomique</span>, ou <span style='font-style:italic;'>recherche par dichotomie</span> (en anglais : <span style='font-style:italic;'>binary search</span>), est un algorithme de recherche pour trouver la position d’un élément dans un tableau trié.
<br><br>
Le but de ce chapitre est de présenter l'algorithme de recherche dichotomique et de l'appliquer à quelques exemples simples.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un point de cours présentant le principe général de l'algorithme, ses spécifications, sa preuve et sa complexité dans le pire des cas ;</li>
<li>des exercices d'entraînement sur la recherche dichotomique.</li>
</ol>
<br><br> 


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Recherche_dichotomique_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','RechercheDichotomique_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>
<span style='font-style:italic;'>Il n'y a pas de ressources à télécharger.</span>
