<h1 style="text-align:center;">Chapitre 9 : dictionnaires </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "dictionnaire.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "hedy.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
Hedwig Kiesler, dite <a href='https://fr.wikipedia.org/wiki/Hedy_Lamarr' target="_blank">Hedy Lamarr </a>, est une actrice, productrice de cinéma et inventrice autrichienne, naturalisée américaine. Elle a co-développé un système de communication appelé &laquo; saut de fréquence &raquo; qui a jeté les bases de la technologie du spectre étalé, utilisée aujourd'hui dans les communications sans fil.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Au chapitre 6, nous avons vu qu'en Python un tableau (au sens usuel du terme) est assimilé à une structure spécifique appelée <span style='font-style:italic;'>liste</span>>
<br><br>
En informatique, il existe d'autres types de tableaux, comme les <span style='font-style:italic;'>tableaux associatifs</span> qui fonctionnent suivant des paires <code>clé:valeur</code>, la <span style='font-style:italic;'>clé</span> correspondant à une entrée du tableau et la <span style='font-style:italic;'>valeur</span> correspondant à la valeur associée. En Python, un tableau associatif est assimilé à une structure spécifique appelée <span style='font-style:italic;'>dictionnaire</span> (type <code>dict</code>).
<br><br>
Le but de ce chapitre est de comprendre et manipuler ce nouveau type de donnée.<br><br>On propose le déroulé suivant :
<ol>
<li>un notebook que l'élève complètera en s'aidant du cours proposé ;</li>
<li>un QCM d'auto-évaluation.</li>
</ol>

<br><br>

<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Dictionnaires.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Booleens_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;display:none;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

 

<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Accès direct via Capytale </h3>

Dans la zone Rechercher, taper la phrase ci-dessous :

    - NSI Première Partie 1 Chapitre 9 Dictionnaires


<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/sceaHM2QPYKo64d" target="_blank">Téléchargement des ressources</a>
