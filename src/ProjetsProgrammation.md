<h1 style="text-align:center;">Projets de programmation Python</h1>

<br>
<div style="text-align: center;"> 
<img src = "projet.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "cynthia.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Cynthia_Breazeal' target="_blank">Cynthia Breazeal</a>
 est une professeur associée d'Arts des médias et Sciences au Massachusetts Institute of Technology (MIT), où elle dirige le Personal Robots Group du MIT Media Lab. Elle a créé le robot social Jibo et a contribué au développement de la robotique humanoïde.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
On propose ci-dessous, uniquement en téléchargement, divers projets de programmation Python.

<br><br>


<script type="text/javascript" src="utils.js"></script>


<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>


<br><hr><br>


<h2> Jeu du morpion </h2>
<div style="text-align: center;"> 
<img src = "morpion.png" alt='image'>
</div>
<br>
<p>Projet guidé au format notebook pour implémenter une version console du jeu du morpion.<br>
  Cliquer <a href='https://nuage03.apps.education.fr/index.php/s/sH2KwEXTNQWzM46' target='_blank'>ici</a> pour télécharger ce projet.</p>
<br><hr><br>

<h2> Jeu du pendu </h2>
<div style="text-align: center;"> 
<img src = "pendu.png" alt='image'>
</div>
<br>
<p>Projet guidé au format notebook pour implémenter une version console du jeu du pendu. Une liste de mots à trouver est fournie sous la forme d'un fichier texte à joindre au notebook.<br>
Cliquer <a href='https://nuage03.apps.education.fr/index.php/s/Zms5Rjb2MF6dk5f' target='_blank'>ici</a> pour télécharger ce projet.</p>
<br><hr><br>


<h2> Jeu découverte </h2>
<div style="text-align: center;"> 
<img src = "projet3.png" alt='image'>
</div>
<br>
<p>Projet d'initiation à la programmation événementielle nécessitant un EDI sur lequel est installé la bibliothèque <code>pygame</code>. Une bibliothèque <code>graphics_nsi</code> et sa documentation sont fournies. Les élèves apprennent les rudiments de la programmation événementielle à travers divers exercices liant interface graphique et interaction avec la souris et le clavier. Un mini-projet guidé est également proposé.<br>
Cliquer <a href='https://nuage03.apps.education.fr/index.php/s/MXj5M685dzi4NWn' target='_blank'>ici</a> pour télécharger ce projet.</p>
<br><hr><br>


<h2> Traitement d'images </h2>
<br>
<div style="text-align: center;"> 
<img src = "traitement.png" alt='image'>
</div>
<br>
<p>Projet guidé au format notebook pour s'initier au traitement d'images. On présente les formats d'images en niveau de gris et on manipule en particulier les images PGM. Les images sont représentées sous la forme de listes de listes.<br>
Cliquer <a href='https://nuage03.apps.education.fr/index.php/s/XXT9g9xr4BE6Nex' target='_blank'>ici</a> pour télécharger ce projet.</p>


