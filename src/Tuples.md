<h1 style="text-align:center;">Chapitre 7 : tuples </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "tuple.jpg" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "elisa.png" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://en.wikipedia.org/wiki/Elisa_Bertino' target="_blank">Elisa Bertino</a>
 est une informaticienne et chercheuse en sécurité informatique et en protection des données. Elle agit en tant que directrice de recherche du CERIAS, le Centre d'éducation et de recherche en assurance et sécurité de l'information, un institut rattaché à l'Université Purdue.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Au chapitre 6, nous avons vu que les listes Python permettent de stocker, à l'instar de tableaux, différentes données afin de pouvoir les utiliser ultérieurement, voire les modifier ou les supprimer.
<br><br>
Dans certains cas, il peut être intéressant d’avoir une structure de données qui puisse stocker plusieurs éléments simultanément comme
dans un tableau, mais qui ne puisse pas être modifié par la suite afin, par exemple, d’éviter certaines erreurs de manipulation.
<br><br>
En Python, ce type de structure est appelé <span style='font-style:italic;'>tuple</span> (type <code>tuple</code>), ou <span style='font-style:italic;'>n-uplets</span> en référence à une structure mathématique analogue. Par construction, ce type de structure est très proche du type <code>list</code> et est non modifiable.
<br><br>
Le but de ce chapitre est de comprendre et manipuler ce nouveau type de donnée.<br><br>On propose le déroulé suivant :
<ol>
<li>un point de cours pour présenter le type <code>tuple</code> ;
<li>deux notebooks : le premier sur les manipulations de base des tuples et le second pour approfondir ce type de donnée ;</li>
<li>un QCM d'auto-évaluation.</li>
</ol>

<br><br>


<div style="text-align: center;"> 
<img src = "cours.png">
</div>

<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Tuples.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Booleens_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;display:none;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>



<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Accès direct via Capytale </h3>

Dans la zone Rechercher, taper les phrases ci-dessous :

    - NSI Première Partie 1 Chapitre 7 Tuples 1/2

    - NSI Première Partie 1 Chapitre 7 Tuples 2/2


<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/PZJS2LnS75XPwRz" target="_blank">Téléchargement des ressources</a>

