<br>
<div style="text-align: center;"> 
<img src = "orga1.png" alt='image'>
</div> 
<br>


<h1>1. Organisation du cours </h1>
Le cours présenté sur ce site est divisé en six parties :

<ol>
    <li>Programmation Python</li>
    <li>Représentation des données</li>
    <li>Architecture des ordinateurs</li>
    <li>Algorithmique avancée</li>
    <li>Réseaux</li>
    <li>IHM et Web</li>
</ol>

Chacune de ces parties est présentée sur une page Web dédiée. On y trouve en particulier des références au <a href='https://eduscol.education.fr/document/30007/download' target="_blank">Bulletin Officiel</a>, ainsi que les différentes notions qui y seront abordées.

Chaque partie est ensuite découpée en plusieurs chapitres.

<span style='font-style:italic;'>Avertissement : le découpage en parties et chapitres qui est proposé ici n'est en aucun cas un découpage &laquo; officiel &raquo; et n'engage que les auteurs de ce cours. De plus, il n'est pas nécessaire de suivre ce cours de manière linéaire (ce que les auteurs ne font d'ailleurs pas dans leur cours...).</span>

<br><br>

<h1> 2. Organisation des chapitres </h1>
Afin d'avoir un ensemble homogène sur l'ensemble des pages du site, tous les chapitres du cours sont organisés de la même façon en plusieurs sections :

<ul>
    <li>une image illustrant le chapitre ;</li>
    <li>le portrait d'une femme informaticienne ;</li>
    <li>une section d'intentions concernant le déroulé du chapitre qui est repérée par l'icône
    <div style="text-align: center;"> 
<br><img src = "intentions.png"><br><br>
</div></li>
    <li>une section de cours dans laquelle on propose des TP, du cours ou des exercices en version pdf téléchargeables, et qui est repérée à l'aide de l'icône
    <div style="text-align: center;"> 
<br><img src = "cours.png"><br><br>
</div></li>
<li>une section de ressources à télécharger, éventuellement vide, qui est repérée par l'icône
<div style="text-align: center;"> 
<br><img src = "logos.png"><br><br>
</div>
Lorsque le chapitre nécessite l'utilisation de notebooks jupyter, ceux-ci peuvent être obtenus directement à l'aide de la bibliothèque partagée de <a href='https://capytale2.ac-paris.fr/web/c-auth/list' target="_blank">Capytale</a> (voir la sous-section <span style='font-style:italic;'>Accès via Capytale</span>).<br>On peut aussi obtenir directement ces notebooks dans la sous-section <span style='font-style:italic;'>Téléchargement</span> qui offre un lien direct sur le nuage de <a href='https://portail.apps.education.fr/' target="_blank">Apps Education</a>. Une fois téléchargés, ceux-ci sont alors visibles sur <a href="https://basthon.fr/" target="_blank"> Basthon</a>.<br>
Les ressources à télécharger peuvent aussi ne pas être des notebooks. Cela peut être des fichiers spécifiques d'exercices (comme par exemple sur les réseaux), ou des documents ressources.<br>
Dans tous les cas, si un lien de téléchargement est proposé, visitez le !
</li>
</ul>



<br><br>

<h1> 3. Prise en main de la plateforme </h1>
<br>
Il faut distinguer deux parties pour une prise en main efficace. La prise en main de la page et celle des fichiers pdf. 
<br><br>

<ol>
<li> Le menu de la page est repéré par les trois symboles suivants :
<div style="text-align: center;"> 
<br><img src = "menu_page.png"><br><br>
</div>
<ul>
    <li>le premier symbole permet de cacher ou montrer la barre de menu :
<div style="text-align: center;"> 
<br><img src = "menu.png"><br><br>
</div>
</li>
    <li>le deuxième permet de changer de couleur :
<div style="text-align: center;"> 
<br><img src = "couleurs.png"><br><br>
</div>
</li>
    <li>le troisième permet de faire une recherche de mots dans le site en dehors des pdf :
<div style="text-align: center;"> 
<br><img src = "recherche.png"><br><br>
</div>
</li>
<br>
</ul>
</li>
<li>Le menu de la partie pdf n'apparaît que lorsque l'on affiche un fichier pdf et se sépare en trois parties :
<ul>
<li>La partie gauche contient essentiellement deux symboles qui nous servirons :
    <ul>
        <li>Le premier symbole permet de montrer ou cacher la barre latérale :
        <div style="text-align: center;"> 
<br><img src = "menu_pdf_1.png"><br><br>
</div>
Lorsque la barre latérale est affichée, on trouve à nouveau deux symboles actifs :
    <ul>
        <li>le premier permet de se déplacer dans le document page par page :
        <div style="text-align: center;"> 
<br><img src = "menu_pdf_2.png"><br><br>
</div>
        </li>
        <li>le second permet de se déplacer dans le document suivant les différents paragraphes du pdf :
        <div style="text-align: center;"> 
<br><img src = "menu_pdf_3.png"><br><br>
</div>
        </li>
    </ul>
</li>
<li>Le second symbole permet de faire une recherche de mots dans les pdf :
<div style="text-align: center;"> 
<br><img src = "menu_pdf_4.png"><br><br>
</div>
</li>
    </ul>
</li>
<li>La partie centrale permet d'effectuer des zooms sur la page :
<div style="text-align: center;"> 
<br><img src = "menu_pdf_6.png"><br><br>
</div>
</li>
<li>La partie droite permet d'effectuer différentes actions importantes : impression, téléchargement, etc.
<div style="text-align: center;"> 
<br><img src = "menu_pdf_5.png"><br><br>
</div> Elle est à découvrir et à explorer suivant les besoins.
</li>
</ul>
</li>
</ol>