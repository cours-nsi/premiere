<h1 style="text-align:center;">Chapitre 2 : variables et types </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "variable.jpg" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "Anita_Borg.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Anita_Borg' target="_blank">Anita Borg</a>
 est une informaticienne américaine. Elle a notamment travaillé pour des compagnies telles Digital Equipment Corporation et Xerox. Elle a également fondé la Grace Hopper Celebration of Women in Computing dans l'Institut Anita Borg (maintenant connu sous le nom de AnitaB.org) pour promouvoir la participation des femmes dans les domaines de la technologie et de l'informatique.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
La notion de variable est très importante en programmation. Dans cette partie , on aborde la notion de variable simple.
<br><br>
Le but de ce chapitre est de comprendre cette notion ainsi que l'affectation d'une donnée à cette variable. Ensuite on aborde la notion de typage d'une donnée.
<br><br>On propose le déroulé suivant :
<ol>
<li>un point de cours pour présenter la notion de variable et affectation ;
<li>un exercice &laquo; à la main &raquo;  et un notebook d'entraînement sur les variables et les affectations ;</li>
<li>un QCM d'auto-évaluation ;</li>
<li>un point de cours pour présenter le typage des données ;</li>
<li>un notebook d'entraînement sur les types ;</li>
</ol>
<br><br>

<div style="text-align: center;"> 
<img src = "cours.png">
</div>


<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','VariablesTypes.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Booleens_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;display:none;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>
<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Accès via Capytale </h3>

Dans la zone Rechercher, taper les phrases ci-dessous :

    - NSI Première Partie 1 Chapitre 2 Variables et affectation

    - NSI Première Partie 1 Chapitre 2 Types


<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/2SLCY4Pr324XW84" target="_blank">Téléchargement des ressources</a>