<h1 style="text-align:center;">Chapitre 7 : exemples d’algorithmes gloutons</h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "greedy.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "Mary.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Mary_Allen_Wilkes' target="_blank">Mary Allen Wilkes</a>
 est une programmeuse et conceptrice de logiciel, connue pour son travail avec l'ordinateur LINC (Laboratory INstrument Computer), considéré comme le premier ordinateur personnel. En janvier 1963, le groupe LINC quitte le laboratoire Lincoln pour créer le Centre de technologie informatique en sciences biomédicales sur le campus de Cambridge, dans le Massachusetts. 
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
En informatique, on rencontre souvent des <span style='font-style:italic;'>problèmes d’optimisation</span>, c’est-à-dire des problèmes pour lesquels on cherche la meilleure solution possible satisfaisant un certain nombre de contraintes.
<br><br>
En pratique, on cherche à trouver une solution algorithmique pour résoudre de tels problèmes lorsque :
<ul>
<li>le problème possède un très grand nombre de solutions ;</li>
<li>on sait évaluer la qualité de chacune des solutions (et donc dire quelle solution est meilleure parmi plusieurs).</li>
</ul>
<br>
Le but de ce chapitre est de présenter une classe d'algorithmes très importante pour la résolution de problèmes d'optimisation : les <span style='font-style:italic;'>algorithmes gloutons</span>.
<br><br>
On propose le déroulé suivant :
<ol>
<li>lecture interactive du cours et résolution des exercices qui y sont intégrés. On aborde en particulier les problèmes d'optimisation classiques suivants :
<ul>
<li>le problème du voyageur de commerce ;</li>
<li>un problème d'organisation ;</li>
<li>le problème du rendu de monnaie.</li>
</ul></li>
<li>des exercices d'entraînement sur les algorithmes gloutons.</li>
</ol>
<br><br> 


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','AlgoGloutons_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','AlgoGloutons_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>
<span style='font-style:italic;'>Il n'y a pas de ressources à télécharger.</span>
