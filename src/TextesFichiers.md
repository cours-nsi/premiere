<h1 style="text-align:center;">Chapitre 8 : textes et fichiers </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "texte.jpg" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "shafi.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Shafi_Goldwasser' target="_blank">Shafi Goldwasser</a>
 est une informaticienne américano-israélienne. Elle est professeure au MIT et à l'Institut Weizmann. Elle a reçu le prix Grace Murray Hopper en 1996, le prix Gödel en 1993 et en 20015 et le prix Turing en 20126, pour ses travaux autour des preuves interactives en théorie de la complexité.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 

<br>
Les chaînes de caractères représentent l'un des type de base en informatique. On les retrouve dans de nombreuses manipulations de base : affichage de texte, gestion du clavier, manipulation de fichiers, etc.
<br><br>
Le but de ce chapitre est de comprendre et manipuler les chaînes de caractères. On commnence par étudier le type de base correspondant (type <code>str</code>), puis on applique ce que l'on vient de voir à la gestion des fichiers de type <span style='font-style:italic;'>texte brut</span> (extension <code>.txt</code>).
<br><br>On propose le déroulé suivant :
<ol>
<li>un point de cours pour présenter le type <code>str</code> ;
<li>un notebook d'entraînement sur la manipulation des chaînes de caractères ;</li>
<li>un QCM d'auto-évaluation ;</li>
<li>un point de cours pour présenter les instructions de base pour la gestion des fichers textes ;
<li>un notebook d'entraînement sur la gestion des fichiers textes ;</li>
<li>un QCM d'auto-évaluation.</li>
</ol>

<br><br>

<div style="text-align: center;"> 
<img src = "cours.png">
</div>

<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','TexteFichiers.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Booleens_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;display:none;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

 

<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Accès via Capytale </h3>
Dans la zone Rechercher, taper les phrases ci-dessous :

    - NSI Première Partie 1 Chapitre 8 Textes

    - NSI Première Partie 1 Chapitre 8 Fichiers

<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/SQ3nggAQ2WWBLpn" target="_blank">Téléchargement des ressources</a>

