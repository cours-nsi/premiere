<h1 style="text-align:center;">Chapitre 3 : représentation des nombres entiers (entiers signés) </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "zorro.png" alt='image'>
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "dwork.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Cynthia_Dwork' target="_blank">Cynthia Dwork</a>
 est une informaticienne américaine, professeur à l'Université Harvard. Elle y occupe la chaire Gordon McKay en informatique ; elle est également Radcliffe Alumnae Professor au Radcliffe Institute for Advanced Study, et professeure affiliée à la Faculté de droit de Harvard. Elle est connue pour ses recherches visant à formuler des fondements mathématiques rigoureux à l'analyse des données préservant la zone privée.
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div>

<br>
Dans le chapitre 2, nous nous sommes intéressés à la représentation binaire des entiers naturels.
<br><br>
Dans ce chapitre, nous nous intéressons à la représentation des nombres entiers (relatifs) également appelés <span style='font-style:italic;'>entiers signés</span> ou encore <span style='font-style:italic;'>integer</span> en anglais.
<br><br>
Il s’agit ici d’étendre la représentation des entiers naturels aux entiers négatifs tout en conservant la représentation des entiers naturels vue au chapitre précédent.
<br><br>
On propose le déroulé suivant :
<ol>
<li>un point de cours présentant deux représentations des entiers signés : l'une par la réservation d'un bit de signe et l'autre par la méthode du complément à deux ;</li>
<li>des exercices d'entraînement pour manipuler les représentations binaires des entiers signés.</li>
</ol>
<br><br>


<div style="text-align: center;"> 
<img src = "cours.png">
</div> 

<div>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Codage_entiers_signes_cours.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Codage_entiers_signes_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>

<br>
<br>
<br>

<div style="text-align: center;"> 
<img src = "logos.png">
</div>
<br>
<span style='font-style:italic;'>Il n'y a pas de ressources à télécharger.</span>
