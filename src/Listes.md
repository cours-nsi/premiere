<h1 style="text-align:center;">Chapitre 6 : listes </h1>

<script type="text/javascript" src="utils.js"></script>
<script>
function affichage_pdf3(elem,nom)
    {   
        elem.style.display="block";
        elem.src = url(nom) + "#zoom=page-width&pagemode=none";
    }

function affichage_iframe(id_bouton,id_frame,nom_pdf)
    {
        let elem_bouton = document.getElementById(id_bouton);
        let elem_frame = document.getElementById(id_frame);
        if(elem_frame.style.display=="none")
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Masquer le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Masquer les exercices';
            }
            window.onload = affichage_pdf3(elem_frame,nom_pdf);
        }
        else
        {
            if (id_bouton == 'cours')
            {
                elem_bouton.innerHTML = 'Afficher le cours';
            }
            else
            {
                elem_bouton.innerHTML = 'Afficher les exercices';
            }
            elem_frame.style.display="none";
        }
    }
</script>

<br>
<div style="text-align: center;"> 
<img src = "listes_2.png">
</div> 
<br>

<div style='border:1px solid black;background-color:#edecec;'>
<br>
<div style="text-align: center;"> 
<img src = "maria.jpg" alt='image'>
</div>
<br>
<p style="background-color:Khaki;margin-left:2%;margin-right:2%;">
<a href='https://fr.wikipedia.org/wiki/Maria_Klawe' target="_blank">Maria Klawe </a>
est une mathématicienne et informaticienne canadienne et américaine. Elle est présidente depuis 2006 de Harvey Mudd College (en), une université scientifique et d'ingénierie californienne, dont elle est la première femme à exercer cette fonction. Elle s'est fortement engagée en faveur de la présence des femmes dans les disciplines STEM
</p>
<br>
</div>

<br>

<div style="text-align: center;"> 
<img src = "intentions.png">
</div> 
<br>
En Python, un tableau est assimilé à une structure spécifique appelée <span style='font-style:italic;'>liste</span> (type <code>list</code>).
<br><br>
Le but de ce chapitre est de comprendre et manipuler ce nouveau type de donnée.<br><br>On propose le déroulé suivant :
<ol>
<li>un premier notebook pour introduire cette nouvelle structure de données ;</li>
<li>un point de cours pour formaliser le type <code>list</code> : définition, indexation, opérateurs et tests ;</li>
<li>deux notebooks d'entraînement pour manipuler les bases des listes, suivis d'un QCM d'auto-évaluation ;</li>
<li>un point de cours où l'on aborde les parcours de listes ;</li>
<li>un notebook d'entraînement sur les parcours de listes, suivi d'un QCM d'auto-évaluation ;</li>
<li>un point de cours pour présenter quelques fonctions et méthodes agissant sur les listes ;</li>
<li>deux notebooks d'entraînement sur ces différentes fonctions, suivis d'un QCM d'auto-évaluation.</li>
</ol>

<br><br>


<div style="text-align: center;"> 
<img src = "cours.png">
</div>

<div class='centre'>

<span id='cours' onclick="affichage_iframe('cours','pdf-js-viewer-test3','Listes.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;'>Afficher le cours</span>
 
<iframe id="pdf-js-viewer-test3" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>

<br><br><br>

<span id="exercices" name = 'exercice' onclick="affichage_iframe('exercices','pdf-js-viewer-test4','Booleens_cours_exercices.pdf');" style='color:blue;text-decoration:underline;cursor:pointer;display:none;'>Afficher les exercices</span>
 
<iframe id="pdf-js-viewer-test4" src="" width="90%" height="800" frameborder="1" style="display:none;"></iframe>
</div>



<div style="text-align: center;"> 
<img src = "logos.png">
</div>

<h3>Accès via Capytale </h3>

Dans la zone Rechercher, taper les phrases ci-dessous :

    - NSI Première Partie 1 Chapitre 6 Listes introduction

    - NSI Première Partie 1 Chapitre 6 Listes définition et indexation

    - NSI Première Partie 1 Chapitre 6 Listes compréhension

    - NSI Première Partie 1 Chapitre 6 Listes parcours

    - NSI Première Partie 1 Chapitre 6 Listes fonctions méthodes

    - NSI Première Partie 1 Chapitre 6 Listes méthode append

<h3>Téléchargement </h3>
<a href="https://nuage03.apps.education.fr/index.php/s/BT6yiRNMgaKYe9J" target="_blank">Téléchargement des ressources</a>